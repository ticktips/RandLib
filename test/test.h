
#ifndef TEST_H_
#define TEST_H_
#include <iostream>
#include<stdlib.h>
#include<stdio.h>
#include <string>
using namespace std;

inline void test_result(bool pass)
{
    if(pass){
        fprintf(stdout, "\033[1;32;40mPass\033[0m  "); 
    } else {
        fprintf(stdout, "\033[1;31;40mFail\033[0m  "); 
    }
}

#define S_(x) #x

#define TEST(func_) {\
    test_result(func_); \
    fprintf(stdout, "%-60s\n", S_(func_)); \
}

#endif
