
#include "myrandlib.h"
#include "test.h"

void test();

int main(int argc, char* argv[])
{
    test();
    return 0;
}

void test()
{
    srand((int) time(0));
    fprintf(stdout, "---- start of test -----------\n\n");


    TEST(is_rand_hit(0) == false);
    TEST(is_rand_hit(100, 100) == true);
    TEST(is_rand_hit(10, 0) == false);
    TEST(is_rand_hit(10, -10) == false);

    TEST(rand_with_range(100, 100) == 100);
    TEST(rand_with_range(0, 0) == 0);
    TEST(rand_with_range(-1, -1) == -1);

    int aarr[] = {50, 0,};
    TEST(rand_with_rate_array(aarr, 2) == 0);

    int barr[] = {0, 50,};
    TEST(rand_with_rate_array(barr, 2) == 1);

    int carr[][2] = {{ 0, 1 }, { 1, 2 },};
    TEST(rand_with_value_rate_array(carr, 2) == 2);

    int darr[][2] = {{ 1, 1 }, { 0, 2 },};
    TEST(rand_with_value_rate_array(darr, 2) == 1);

    int earr[][3] = {{ 0, 1, 3 }, { 0, 2, 4 },{1, 3, 5}};
    TEST(rand_with_rate_array_by_step((int*)earr, 3, 3) == 2);

    TEST(rand_bit_off_index(1,2) == 2);
    TEST(rand_bit_off_index(127,8) == 8);
    TEST(rand_bit_off_index(2, 0, 2) == 1);
    TEST(rand_bit_off_index(7, 0, 4) == 4);

    TEST(rand_bit_on_index(2,2) == 2);
    TEST(rand_bit_on_index(128,8) == 8);
    TEST(rand_bit_on_index(1, 0, 1) == 1);
    TEST(rand_bit_on_index(4, 2, 3) == 3);

    std::vector<int> result;
    TEST((rand_n_result_from_range(1, 1, 1, result), 
                result == std::vector<int>(1, 1)));
    TEST((rand_n_result_from_range(1, 9, 9, result), 
                result == std::vector<int>(1, 9)));

    int arr[]={1,2,3,4,5,6,7,8};
    TEST((rand_n_result_from_array(1, (int*)arr, 1, result), 
                result == std::vector<int>(1, 1)));
    TEST((rand_n_result_from_array(2, (int*)arr, 1, result), 
                result == std::vector<int>(1, 1)));
    fprintf(stdout, "\n---- end of test -----------\n\n");
}
